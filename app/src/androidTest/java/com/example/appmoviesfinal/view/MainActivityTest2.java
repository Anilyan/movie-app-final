package com.example.appmoviesfinal.view;

import android.view.View;
import android.widget.TextView;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.annotation.UiThreadTest;
import androidx.test.rule.ActivityTestRule;
import com.example.appmoviesfinal.R;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.hamcrest.core.IsNull.notNullValue;

public class MainActivityTest2 {

    @Rule
    public ActivityTestRule<MainActivity> activityResult = new ActivityTestRule<>(MainActivity.class);
    private MainActivity mainActivity = null;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception {
        mainActivity = activityResult.getActivity();
    }
    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }

    @UiThreadTest
    @Test
    public void ensureConnectionMsgIsShownIfIsNotConnected() {
        TextView viewById = mainActivity.findViewById(R.id.connection_msg);
        Assert.assertThat(viewById, notNullValue());
        mainActivity.setDataAccordingToConnection(false, viewById);
        assertEquals(viewById.getText(), "Please verify your internet connection");
        assertEquals(View.VISIBLE, viewById.getVisibility());
    }

    @UiThreadTest
    @Test
    public void ensureConnectionMsgIsHiddenIfIsConnected() {
        TextView viewById = mainActivity.findViewById(R.id.connection_msg);
        Assert.assertThat(viewById, notNullValue());
        mainActivity.setDataAccordingToConnection(true, viewById);
        assertEquals(View.INVISIBLE, viewById.getVisibility());
    }

}

