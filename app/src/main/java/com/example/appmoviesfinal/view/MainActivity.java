package com.example.appmoviesfinal.view;

import android.app.SearchManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.appmoviesfinal.R;
import com.example.appmoviesfinal.adapter.MovieAdapter;
import com.example.appmoviesfinal.utils.ConnectionApplication;
import com.example.appmoviesfinal.utils.ConnectivityReceiver;
import com.example.appmoviesfinal.viewModel.MovieViewModel;

public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private RecyclerView movieRecyclerView;
    private MovieAdapter adapter;
    private Handler handler = new Handler();
    private MovieViewModel movieViewModel;
    private GridLayoutManager layoutManager;

    private Integer pageCounter = 1;
    private boolean isLoading = false;
    private SwipeRefreshLayout mySwipeRefreshLayout;

    private static Integer LIST_LAST_POSITION = 20;
    private static Integer LIST_FIRST_POSITION = 0;
    private static Integer FIRST_PAGE = 1;
    private static int x;

    private boolean isSearch = false;
    private String movieName = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        initAndSetAdapter();

        populateData(); //only happens if there is connection
        initRefresh();
        initScrollListener();
        initAnimator();

    }

    private void initRecyclerView() {
        movieRecyclerView = findViewById(R.id.movie_container);
        movieRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        movieRecyclerView.setHasFixedSize(true);
    }

    private void initAndSetAdapter() {
        adapter = new MovieAdapter();
        movieRecyclerView.setAdapter(adapter);
    }

    public void populateData() {
        boolean isConnected = checkConnection();
        if(isConnected) {
            movieRecyclerView.scrollToPosition(0);
            movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
            movieViewModel.moviesByPage(pageCounter).observe(this, adapter::setMovies);
        }
    }

    public void populateWithSearchData(String movieName) {
        boolean isConnected = checkConnection();
        if(isConnected) {
            movieRecyclerView.scrollToPosition(0);
            movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
            movieViewModel.moviesByName(pageCounter, movieName).observe(this, adapter::setMovies);
        }
    }

    private void setDataAccordingToSearchType() {
        if (isSearch) {
            populateWithSearchData(movieName);
        } else {
            populateData();
        }
    }

    public void posterOnClick(View v) {
        DialogFragment newFragment = adapter.createPosterDialogFragment(this);
        newFragment.show(getSupportFragmentManager(), "PosterDialogFragment");
    }

    /**
     * Creates an animation, Just for fun
     */
    private void initAnimator() {
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        movieRecyclerView.setItemAnimator(itemAnimator);
    }

    /*
     * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
     * performs a swipe-to-refresh gesture.
     */
    private void initRefresh(){
        mySwipeRefreshLayout = findViewById(R.id.swipeContainer);
        mySwipeRefreshLayout.setOnRefreshListener(
                () -> {
                    setDataAccordingToSearchType();
                    mySwipeRefreshLayout.setRefreshing(false);
                }
        );
    }

    /**
     * Method to manually check connection status
     */
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        TextView msgView = findViewById(R.id.connection_msg);
        return setDataAccordingToConnection(isConnected, msgView);
    }

    /**
     * Updates recycler view if there is connection, otherwise shows instruction
     * @param isConnected if connection is true
     */
    public boolean setDataAccordingToConnection(boolean isConnected, TextView msgView) {
        if (isConnected) {
            msgView.setHeight(0);
            msgView.setVisibility(View.INVISIBLE);
            //initScrollListener();
            return true;
        } else {
            String message = "Please verify your internet connection";
            msgView.setHeight(100);
            msgView.setVisibility(View.VISIBLE);
            msgView.setText(message);
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is a change in network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        TextView msgView = findViewById(R.id.connection_msg);
        setDataAccordingToConnection(isConnected, msgView);
    }

    /**
     * If the bottom-most item is visible we repopulate the list
     */
    private void initScrollListener() {
        movieRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                x=dx;
                layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == LIST_LAST_POSITION - 1) {
                        pageCounter++; //at bottom of list!
                        load();
                        isLoading = true;
                    }
                    if (layoutManager.findFirstCompletelyVisibleItemPosition() == LIST_FIRST_POSITION && pageCounter != FIRST_PAGE) {
                        pageCounter--; //at top of list!
                        load();
                        isLoading = true;
                    }
                }
            }
        });
    }

    /**
     * Populates recycler view with movies from a new page of the api
     */
    private void load() {
        setDataAccordingToSearchType();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);
    }


    /**
     * Inflate the menu; this adds items to the action bar if it is present
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        initSearch(menu);
        return true;
    }

    /**
     * Associates to each menu item its respective behaviour
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.home:
                pageCounter = FIRST_PAGE;
                populateData();
                return(true);
            case R.id.about:
                Toast.makeText(this, "sample toast", Toast.LENGTH_LONG).show();
                return(true);
        }
        return(super.onOptionsItemSelected(item));
    }

    public void initSearch(Menu menu) {
        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSearch = true;
                movieName = query;
                populateWithSearchData(query);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }
}

