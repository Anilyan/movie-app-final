package com.example.appmoviesfinal.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.appmoviesfinal.BuildConfig;
import com.example.appmoviesfinal.api.MoviesApi;
import com.example.appmoviesfinal.api.RetrofitGsonApiImplementer;
import com.example.appmoviesfinal.model.Movie;
import com.example.appmoviesfinal.model.MovieDBApiResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRepository {

    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private MoviesApi moviesApi = new RetrofitGsonApiImplementer<>(BASE_URL, MoviesApi.class).create();
    private static MovieRepository instance;

    public LiveData<List<Movie>> moviesUntilGivenPage = new MutableLiveData<>();

    private MovieRepository() {
        // singleton instance must be acquired through @code{getInstance()} invocation
    }

    public static MovieRepository getInstance() {
        if (instance == null) instance = new MovieRepository();
        return instance;
    }

    /*
     * Fetches movies from the provided data source by page
     * */
    public LiveData<List<Movie>> fetchTopMoviesByPage(String page) {
        MutableLiveData<List<Movie>> movies = new MutableLiveData<>();

        moviesApi.getMoviesByPage(BuildConfig.ApiKey, "en-US", page)
                .enqueue(new Callback<MovieDBApiResponse>() {
            @Override
            public void onResponse(Call<MovieDBApiResponse> call, Response<MovieDBApiResponse> response) {
                if (!response.isSuccessful()) {
                    try {
                        throw new Exception("Server unreachable : Error " + response.code());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }
                if (response.body() != null) movies.setValue(response.body().getMovies());
                else movies.setValue(new ArrayList<>());
            }

            @Override
            public void onFailure(Call<MovieDBApiResponse> call, Throwable t) {
                //todo handle failure
                try {
                    throw t;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    System.exit(255);
                }
            }
        });
        return movies;
    }

    /*
     * Fetches movies from the provided data source by name/query
     * */
    public LiveData<List<Movie>> searchMovieByName(String page, String movieName) {
        MutableLiveData<List<Movie>> movies = new MutableLiveData<>();

        moviesApi.getMovieByName(BuildConfig.ApiKey, movieName, "en-US", page)
                .enqueue(new Callback<MovieDBApiResponse>() {
                    @Override
                    public void onResponse(Call<MovieDBApiResponse> call, Response<MovieDBApiResponse> response) {
                        if (!response.isSuccessful()) {
                            try {
                                throw new Exception("Server unreachable : Error " + response.code());
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                        if (response.body() != null) movies.setValue(response.body().getMovies());
                        else movies.setValue(new ArrayList<>());
                    }

                    @Override
                    public void onFailure(Call<MovieDBApiResponse> call, Throwable t) {
                        //todo handle failure
                        try {
                            throw t;
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            System.exit(255);
                        }
                    }
                });
        return movies;
    }


}
