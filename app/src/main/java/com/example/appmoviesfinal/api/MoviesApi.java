package com.example.appmoviesfinal.api;

import com.example.appmoviesfinal.model.MovieDBApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesApi {

    @GET("movie/top_rated")
    Call<MovieDBApiResponse> getMoviesByPage(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page
    );

    @GET("search/movie")
    Call<MovieDBApiResponse> getMovieByName(
            @Query("api_key") String api_key,
            @Query("query") String query,
            @Query("language") String language,
            @Query("page") String page
    );

}