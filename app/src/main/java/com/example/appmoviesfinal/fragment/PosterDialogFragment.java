package com.example.appmoviesfinal.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;

import com.example.appmoviesfinal.R;
import com.example.appmoviesfinal.model.Movie;
import com.squareup.picasso.Picasso;

public class PosterDialogFragment extends DialogFragment {
    private AlertDialog.Builder builder;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.dialog_poster)
                .setNegativeButton(R.string.close, (dialog, id) -> {
                    // User cancelled the dialog
                    PosterDialogFragment.this.getDialog().cancel();
                });
        return builder.create();
    }

    public void setView(Activity activity, Movie movie){
        View popUpView = activity.getLayoutInflater().inflate(R.layout.dialog_poster, null); // inflating popup layout
        ImageView imgView = popUpView.findViewById(R.id.poster_big);
        Picasso.get()
                .load("https://image.tmdb.org/t/p/w500/" + movie.getPoster())
                .placeholder(R.drawable.poster_placeholder)
                .error(R.drawable.poster_placeholder)
                .into(imgView);
//        builder.setView(popUpView); //ToDo fix erro
    }
}
