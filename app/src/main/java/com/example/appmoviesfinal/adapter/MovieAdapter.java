package com.example.appmoviesfinal.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appmoviesfinal.R;
import com.example.appmoviesfinal.fragment.PosterDialogFragment;
import com.example.appmoviesfinal.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private List<Movie> movies = new ArrayList<>();
    private Activity activity;
    private MovieHolder movieHolder;
    private Movie movieSet;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.movie, parent, false);
            return new MovieHolder(view);
        } else {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.loading_item, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieHolder) {
            Movie currentMovie = movies.get(position);
            setMovie((MovieHolder) holder, currentMovie);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    private void setMovie(@NonNull MovieHolder holder, Movie movie) {
        holder.title.setText(movie.getTitle());
        this.movieHolder=holder;
        this.movieSet=movie;
        //using Picasso library for easy image caching and loading from the web
        Picasso.get()
                .load("https://image.tmdb.org/t/p/w500/" + movie.getPoster())
                .placeholder(R.drawable.poster_placeholder)
                .error(R.drawable.poster_placeholder)
                .into(holder.poster);
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public int getItemViewType(int position) {
        return movies.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOADING;
    }

    private class MovieHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView poster;

        MovieHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            poster = itemView.findViewById(R.id.poster);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        viewHolder.progressBar.setIndeterminate(true);
    }

    public DialogFragment createPosterDialogFragment(Activity activity) {
        PosterDialogFragment newFragment = new PosterDialogFragment();
        newFragment.setView(activity, movieSet);
        return newFragment;
    }
}

