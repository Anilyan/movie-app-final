package com.example.appmoviesfinal.viewModel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.appmoviesfinal.model.Movie;
import com.example.appmoviesfinal.repository.MovieRepository;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {

    private MovieRepository repository = MovieRepository.getInstance();
    private LiveData<List<Movie>> moviesByPage;

    public MovieViewModel(@NonNull Application application) {
        super(application);
    }

    /**
     * Exposes movies contained in this {@link AndroidViewModel}
     * @return movies by page
     */
    public LiveData<List<Movie>> moviesByPage(int page) {
        moviesByPage = repository.fetchTopMoviesByPage(Integer.toString(page));
        return moviesByPage;
    }

    /**
     * Exposes movies contained in this {@link AndroidViewModel}
     * @return movies by name
     */
    public LiveData<List<Movie>> moviesByName(int page, String movieName) {
        moviesByPage = repository.searchMovieByName(Integer.toString(page), movieName);
        return moviesByPage;
    }


}