package com.example.appmoviesfinal.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    @Test
    public void ensureMovieTitleIsCorrectlyRetrieved() {
        Movie movie = new Movie("Name", "wtv");
        String titleRetrieved = movie.getTitle();
        Assert.assertEquals("Name", titleRetrieved);
    }
}